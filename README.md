# HmongLBM Citelis Plugin

## Overview

This plugin integrated [Citelis Payments] with Sylius based applications. After installing it you should be able to create a payment method for Citelis gateway and enable its payments in your web store.

## Support

We work on amazing eCommerce projects on top of Sylius.

## Installation

```bash
$ composer require hmonglee/citelis-plugin

```
    
Add plugin dependencies to your AppKernel.php file:

```php
public function registerBundles()
{
    return array_merge(parent::registerBundles(), [
        ...
        
        new \HmongLBM\SyliusCitelisPlugin\HmongLBMSyliusCitelisPlugin(),
    ]);
}
```

## Testing
```bash
$ wget http://getcomposer.org/composer.phar
$ php composer.phar install
$ yarn install
$ yarn run gulp
$ php bin/console sylius:install --env test
$ php bin/console server:start --env test
$ open http://localhost:8000
$ bin/behat features/*
$ bin/phpspec run
```

## Contribution

Learn more about our contribution workflow on http://docs.sylius.org/en/latest/contributing/.
