<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin\Exception;

use Payum\Core\Exception\Http\HttpException;

final class CitelisException extends HttpException
{
    const LABEL = 'CitelisException';

    public static function newInstance($status)
    {
        $parts = [self::LABEL];

        if (property_exists($status, 'statusLiteral')) {
            $parts[] = '[reason literal] ' . $status->statusLiteral;
        }

        if (property_exists($status, 'statusCode')) {
            $parts[] = '[status code] ' . $status->statusCode;
        }

        if (property_exists($status, 'statusDesc')) {
            $parts[] = '[reason phrase] ' . $status->statusDesc;
        }

        $message = implode(PHP_EOL, $parts);

        return new static($message);
    }
}
