<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

final class CitelisGatewayConfigurationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('merchant_id', TextType::class, [
                'label' => 'Merchant Id',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('capture_mode', ChoiceType::class, [
                'label' => 'Capture mode',
                'choices' => [
                    'Défaut' => 'AUTHOR_CAPTURE',
                    'Payment en plusieurs fois' => 'PAYMENT_N',
                ],
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('nb_payment', IntegerType::class, [
                'label' => 'Nombre de paiements en plusieurs fois',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('period', IntegerType::class, [
                'label' => 'Nombre de jours entre les paiements',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('capture_day', IntegerType::class, [
                'label' => 'Capture day',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('currency_code', IntegerType::class, [
                'label' => 'Currency code',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
            ->add('merchant_country', TextType::class, [
                'label' => 'Country',
                'constraints' => [
                    new NotBlank([
                        'groups' => ['sylius'],
                    ])
                ],
            ])
        ;
    }
}
