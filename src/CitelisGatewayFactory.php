<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin;

use HmongLBM\SyliusCitelisPlugin\Action\CaptureAction;
use HmongLBM\SyliusCitelisPlugin\Action\ConvertPaymentAction;
use HmongLBM\SyliusCitelisPlugin\Action\StatusAction;
use HmongLBM\SyliusCitelisPlugin\Bridge\CitelisBridgeInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

final class CitelisGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults([
            'payum.factory_name' => 'citelis',
            'payum.factory_title' => 'Citelis',
            'payum.action.capture' => new CaptureAction(),
            'payum.action.convert_payment' => new ConvertPaymentAction(),
            'payum.action.status' => new StatusAction(),
        ]);

        if (false == $config['payum.api']) {
            $config['payum.default_options'] = [
                'merchant_id' => '',
                'merchant_country' => '',
                'currency_code' => '',
                'cgi_bin_dir' => '',
                'capture_mode' => '',
                'capture_day' => '0',
                'nb_payment' => '',
                'period' => '',
            ];
            $config->defaults($config['payum.default_options']);

            if (CitelisBridgeInterface::CAPTURE_MODE_PAYMENT_N === $config['capture_mode']) {
                $config['payum.required_options'] = ['merchant_id', 'merchant_country', 'currency_code', 'cgi_bin_dir', 'capture_day', 'nb_payment', 'period'];
            } else {
                $config['payum.required_options'] = ['merchant_id', 'merchant_country', 'currency_code', 'cgi_bin_dir', 'capture_day'];
            }

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                return [
                    'merchant_id' => $config['merchant_id'],
                    'merchant_country' => $config['merchant_country'],
                    'currency_code' => $config['currency_code'],
                    'cgi_bin_dir' => $config['cgi_bin_dir'],
                    'capture_day' => $config['capture_day'],
                    'capture_mode' => $config['capture_mode'],
                    'nb_payment' => $config['nb_payment'],
                    'period' => $config['period'],
                ];
            };
        }
    }
}
