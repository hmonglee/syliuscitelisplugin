<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin\Bridge;

interface CitelisBridgeInterface
{
    // NEW VERSION

    //Autorisation acceptée
    const RESPONSE_CODE_00000 = '00000';
    const RESPONSE_CODE_02500 = '02500';
    const CAPTURE_MODE_PAYMENT_NX = 'NX';
    const CAPTURE_MODE_PAYMENT_CPT = 'CPT';

    const PAYMENT_CODE_101 = 101;
    const PAYMENT_CODE_125 = 125;

    const BILLING_CYCLE_MONTHLY = 40;

    const CURRENCY_EUR = 978;

    // OLD VERSION
    const CAPTURE_MODE_AUTHOR_CAPTURE = 'AUTHOR_CAPTURE';
    const CAPTURE_MODE_PAYMENT_N = 'PAYMENT_N';

    // Autorisation acceptée
    const RESPONSE_CODE_0 = '0';
    const RESPONSE_CODE_00 = '00';

    // Demande d'autorisation par téléphone à la banque à cause d'un dépassement de plafond d'autorisation sur la carte
    const RESPONSE_CODE_02 = '02';

    // Champ 'merchant_id' invalide
    const RESPONSE_CODE_03 = '03';

    // Autorisation refusée
    const RESPONSE_CODE_05 = '05';

    // Transaction invalide, vérifier les paramètres transférés dans la requête
    const RESPONSE_CODE_12 = '12';

    // Annulation de l'internaute
    const RESPONSE_CODE_17 = '17';

    // Erreur de format
    const RESPONSE_CODE_30 = '30';

    // Suspicion de fraude
    const RESPONSE_CODE_34 = '34';

    // Nombre de tentatives de saisie du numéro de carte dépassé
    const RESPONSE_CODE_75 = '75';

    // Service temporairement indisponible
    const RESPONSE_CODE_90 = '90';
}