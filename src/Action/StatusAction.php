<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin\Action;

use DateTime;
use HmongLBM\SyliusCitelisPlugin\Bridge\CitelisBridgeInterface;
use Payline\PaylineSDK;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Request\GetStatusInterface;

final class StatusAction implements ApiAwareInterface, ActionInterface
{
    /** @var array  */
    private $api = [];

    /**
     * {@inheritDoc}
     */
    public function setApi($api)
    {
        if (!is_array($api)) {
            throw new UnsupportedApiException('Not supported.');
        }

        $this->api = $api;
    }

    /**
     * {@inheritDoc}
     */
    public function execute($request)
    {
        $data = [];

        // GET TOKEN
        if (isset($_POST['token'])) {
            $data['token'] = $_POST['token'];
        } elseif(isset($_GET['token'])) {
            $data['token'] = $_GET['token'];
        } elseif(isset($_GET['paylinetoken'])) {
            $data['token'] = $_GET['paylinetoken'];
        } else {
            $request->markNew();

            return;
        }

        // VERSION
        if (isset($_POST['version'])) {
            $data['version'] = $_POST['version'];
        } else {
            $data['version'] = $_SESSION['WS_VERSION'];
        }

        $paylineSDK = new PaylineSDK(
            $_SERVER['PAYLINE_MERCHANT_ID'],
            $_SERVER['PAYLINE_MERCHANT_ACCESS_KEY'],
            null,
            null,
            null,
            null,
            $_SERVER['PAYLINE_ENVIRONMENT'],
            __DIR__.'/../../../../../var/log/'
        );

        $response = $paylineSDK->getWebPaymentDetails($data);
        $response_code = $response['result']['code'];
        $capture_mode = $response['payment']['mode'];
        $card_number = $response['card']['number'];

        if (CitelisBridgeInterface::CAPTURE_MODE_PAYMENT_NX === $capture_mode) {
            $originalPayment = $request->getFirstModel();
            $className = get_class($originalPayment);

            $currencyCode = $originalPayment->getCurrencyCode();
            $method = $originalPayment->getMethod();
            $state = $originalPayment->getState();

            $order = $originalPayment->getOrder();
            $order->removePayments();

            foreach ($response['billingRecordList']['billingRecord'] as $billingRecord) {
                $payment = new $className();
                $payment->setAmount((int) $billingRecord['amount']);
                $payment->setCurrencyCode($currencyCode);
                $payment->setMethod($method);
                $payment->setOrder($order);
                $payment->setState($state);
                $payment->setCreatedAt(new DateTime());
                $payment->setUpdatedAt(new DateTime());
                $payment->setCardNumber($card_number);

                if (isset($billingRecord['transaction']['id'])) {
                    $payment->setTransactionNumber($billingRecord['transaction']['id']);
                }

                $dateTime = DateTime::createFromFormat('d/m/Y H:i', $billingRecord['date']);

                if ($dateTime) {
                    $payment->setTransactionDate($dateTime);
                }

                $payment->setTransactionContent($response);
                $order->addPayment($payment);
            }
        } else {
            $payment = $request->getFirstModel();
            $payment->setCardNumber($card_number);
            $payment->setTransactionNumber($response['transaction']['id']);
            $payment->setTransactionCertificat($response['authorization']['number']);
            $dateTime = DateTime::createFromFormat('d/m/Y H:i', $response['authorization']['date']);

            if ($dateTime) {
                $payment->setTransactionDate($dateTime);
            }

            $payment->setTransactionContent($response);
        }

        if (in_array($response_code, [
            CitelisBridgeInterface::RESPONSE_CODE_0,
            CitelisBridgeInterface::RESPONSE_CODE_00,
            CitelisBridgeInterface::RESPONSE_CODE_00000,
            CitelisBridgeInterface::RESPONSE_CODE_02500,
        ])) {
            $request->markCaptured();

            return;
        }

        if (CitelisBridgeInterface::RESPONSE_CODE_02 === $response_code) {
            $request->markSuspended();

            return;
        }

        $request->markCanceled();
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof GetStatusInterface &&
            $request->getModel() instanceof \ArrayAccess
        ;
    }
}
