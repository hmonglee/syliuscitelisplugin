<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin\Action;

use HmongLBM\SyliusCitelisPlugin\Bridge\CitelisBridgeInterface;
use HmongLBM\SyliusCitelisPlugin\SetCitelis;
use Payline\PaylineSDK;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\Payum;
use Payum\Core\Reply\HttpPostRedirect;

final class CitelisAction implements ApiAwareInterface, ActionInterface
{
    private $api = [];

    /** @var Payum */
    private $payum;

    /**
     * {@inheritDoc}
     */
    public function setApi($api)
    {
        if (!is_array($api)) {
            throw new UnsupportedApiException('Not supported.');
        }

        $this->api = $api;
    }

    /**
     * @param Payum $payum
     */
    public function __construct(Payum $payum)
    {
        $this->payum = $payum;
    }

    /**
     * {@inheritDoc}
     */
    public function execute($request)
    {
        $paylineSDK = new PaylineSDK(
            $_SERVER['PAYLINE_MERCHANT_ID'],
            $_SERVER['PAYLINE_MERCHANT_ACCESS_KEY'],
            null,
            null,
            null,
            null,
            $_SERVER['PAYLINE_ENVIRONMENT'],
            __DIR__.'/../../../../../var/log/'
        );
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());
        $order = $model->get('order');

        $notify = $this->payum->getTokenFactory()->createNotifyToken(
            $request->getToken()->getGatewayName(),
            $request->getToken()->getDetails()
        );

        $data = [
            'payment' => [
                'amount' => $order->getTotal(),
                'currency' => CitelisBridgeInterface::CURRENCY_EUR,
                'action' => CitelisBridgeInterface::PAYMENT_CODE_101,
                'mode' => CitelisBridgeInterface::CAPTURE_MODE_PAYMENT_CPT,
                'contractNumber' => $_SERVER['PAYLINE_CONTRACT_NUMBER'],
            ],
            'order' => [
                'ref' => $order->getNumber(),
                'country' => $order->getShippingAddress()->getCountryCode(),
                'amount' => $order->getTotal(),
                'currency' => CitelisBridgeInterface::CURRENCY_EUR,
                'date' => date_format($order->getCheckoutCompletedAt(), 'd/m/Y H:i')
            ],
            'returnURL' => $request->getToken()->getAfterUrl(),
            'cancelURL' => $request->getToken()->getAfterUrl(),
            'notificationURL' => $notify->getTargetUrl(),
        ];

        if (CitelisBridgeInterface::CAPTURE_MODE_PAYMENT_N === $this->api['capture_mode']) {
            $nbPayment = $this->api['nb_payment'];
            $amount = round($order->getTotal() / $nbPayment);

            $recurring = [
                'firstAmount' => $order->getTotal() - ($amount * ($nbPayment - 1)),
                'amount' =>  $amount,
                'billingCycle' => CitelisBridgeInterface::BILLING_CYCLE_MONTHLY,
                'billingLeft' => $nbPayment
            ];

            $data['recurring'] = $recurring;
            $data['payment']['action'] = CitelisBridgeInterface::PAYMENT_CODE_125;
            $data['payment']['mode'] = CitelisBridgeInterface::CAPTURE_MODE_PAYMENT_NX;
        }

        $response = $paylineSDK->doWebPayment($data);

        throw new HttpPostRedirect($response['redirectURL']);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return $request instanceof SetCitelis
            && $request->getModel() instanceof \ArrayObject;
    }
}
