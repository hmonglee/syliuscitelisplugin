<?php

declare(strict_types=1);

namespace HmongLBM\SyliusCitelisPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class HmongLBMSyliusCitelisPlugin extends Bundle
{
    use SyliusPluginTrait;
}
